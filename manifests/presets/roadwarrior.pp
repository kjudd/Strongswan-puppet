# Class: strongswan::presets::roadwarrior
#
# Creates Strongswan roadwarrior config.
# Using IKEV2 with key authentication.
# By default this set up will need certs and keys to be created.
# 
#
# = parameters = 
#
# https://wiki.strongswan.org/projects/strongswan/wiki/ConnSection
#
#
class strongswan::presets::roadwarrior (

  $ikelifetime    = '60m',
  $keylife        = '20m',
  $dns            = [ '8.8.8.8', '8.8.4.4' ],
  $right          = '%any',
  $right_sourceip = '172.16.100.0/24',
  $left_subnet    = '0.0.0.0/0',
  $servercert     = 'servercert.pem',
  $serverkey      = 'serverkey.key',
  $auto           = 'add',
  $rekeymargin    = '3m',
  $keyingtries    = '1',
  $keyexchange    = 'ikev2',
  $left           = '%defaultroute',
  $leftauth       = 'pubkey',
  $leftsendcert   = 'always',
  $dpdaction      = 'clear',
  $dpddelay       = '60s',

){
  $_params = {
    'ikelifetime'   => $ikelifetime,
    'auto'          => $auto,
    'keylife'       => $keylife,
    'rekeymargin'   => $rekeymargin,
    'keyingtries'   => $keyingtries,
    'keyexchange'   => $keyexchange,
    'left'          => $left,
    'leftauth'      => $leftauth,
    'leftsendcert'  => $leftsendcert,
    'leftcert'      => $servercert,
    'leftsubnet'    => $left_subnet,
    'right'         => $right,
    'rightsourceip' => $right_sourceip,
    'rightdns'      => join($dns, ','),
    'dpdaction'     => $dpdaction,
    'dpddelay'      => $dpddelay,
  }


  strongswan::conn { 'roadwarrior':
    params  => $_params,
    secrets => { 'RSA'  => $serverkey},
  }
}
