# Creates Strongswan connections 
#
# parameters 
#
#  = $params = 
#  Takes hash items to keys settings files
#  example 
#strongswan::conn { 'example':
#  $params = {
#    'ikelifetime'   => 60m,
#    'auto'          => add,
#    'keylife'       => 20m,
#    'rekeymargin'   => 3m,
#    'keyingtries'   => 1,
#    'keyexchange'   => ikev2,
#    'left'          => %any,
#    'leftauth'      => pubkey,
#    'leftsendcert'  => always,
#    'leftcert'      => servercert.pem,
#    'leftsubnet'    => 0.0.0.0,
#    'right'         => %any,
#    'rightsourceip' => 192.168.1.0/24,
#    'rightdns'      => [ '8.8.8.8', '8.8.4.4' ]
#    'dpdaction'     => clear,
#    'dpddelay'      => 60s,
#    }
#
#  $secrets  = { 'RSA'  => $serverkey, }
#}
#
#
# Authors
# -------
#
# Kylie Judd <Kylie.Judd@gmail.com>
#
define strongswan::conn(
  $params,
  $secrets
) {
  validate_hash($params)
  validate_hash($secrets)

  include strongswan

  $conn_dir = $strongswan::ipsec_conn_dir
  $secrets_dir = $strongswan::ipsec_secrets_dir

  File {
    mode   => '0600',
    owner  => 'root',
    group  => 'root',
    ensure => 'file',
  }

  file {"${conn_dir}/ipsec.${name}.conf":
    content => template('strongswan/ipsec.conn.erb'),
    notify  => Class['strongswan::service'],
    require => File["$conn_dir"],
  }

  file {"${secrets_dir}/ipsec.${name}.secrets.conf":
    content => template('strongswan/ipsec.secrets.erb'),
    notify  => Class['strongswan::service'],
    require => File["$secrets_dir"],
  }

}
