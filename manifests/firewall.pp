
# class Strongswan::firewall
#
# Sets firewall rules to allow incoming connections and enable the host to forward traffic
#
# Parameters
#
# = $client_source_ip =
# which incoming ip addresses to allow
#
# = $ipec_ports =
# Which ports to allow
#
# = $outiface =
# Which interface to allow outgoing forwards.
#
# Authors
# -------
#
# Kylie Judd <Kylie.Judd@gmail.com>
#

class strongswan::firewall(
  $client_source_ip = '0.0.0.0/0',
  $ports            = $strongswan::ports,  #Array
  $outiface         = $networking[primary]
){

  firewall { '021 vpn-client inbound udp':
    proto        => 'udp',
    action       => 'accept',
    source       => "$client_source_ip",
    dport        => $ports,
  }

  firewall {'022 vpn-client masquerading':
    chain        => 'POSTROUTING',
    jump         => 'MASQUERADE',
    proto        => 'all',
    outiface     => "$outiface",
    table        => 'nat',
  }

  firewall {'023 vpn-client ipsec routing policy':
    chain        => 'POSTROUTING',
    action       => 'accept',
    proto        => 'all',
    outiface     => "$outiface",
    ipsec_policy => 'ipsec',
    ipsec_dir    => 'out', 
    table        => 'nat',
  }
}

