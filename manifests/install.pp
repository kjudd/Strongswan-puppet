# Class Strongswan::install
#
# Installs the Strongswan package
#
# Optional paremeters
#
# = $package =
# The name of the package to be installed
#
# = $version =
# Version of package to install
#
#
#
class strongswan::install(

  $plugins = $strongswan::plugins, #array

)
{
  package { "$strongswan::package":
    ensure => "$strongswan::version",
  }
  package { $plugins:
    ensure => "$strongswan::version",
  }
}
