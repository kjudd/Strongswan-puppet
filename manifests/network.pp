# class Strongswan::network
# configures packet forwarding
#
#

class strongswan::network {
  sysctl { 'net.ipv4.ip_forward': value => '1' } 
}

