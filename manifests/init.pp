# Class: strongswan
# ===========================
#
# Installs Strongswan ipsec vpn
#
# Parameters
#
# = $service_name =
#  The name of the service we are expecting to manage 
#
# = $service_ensure =
#  The state of the service we expect e.g. 'running' 
#
# = $service_enable =
#  The state of the service at boot time, e.g. 'true'
#
# = $strongswan_package =
# The name of the package to install
#
# = $strongswan_version =
# The version of Strongswan to install
#
# Authors
# -------
#
# Kylie Judd <Kylie.Judd@gmail.com>
#
#
class strongswan (
  $service_name       = 'strongswan',
  $service_ensure     = 'running',
  $service_enable     = 'true',
  $ports              = ['4500', '500'],
  $sysctl             = '/etc/sysctl.conf',
  $package            = 'strongswan',
  $plugins            = ['strongswan-libipsec' , 'strongswan-charon-nm'],
  $version            = 'latest',
  $ipsec_conf         = $strongswan::params::strongswan_ipsec,
  $ipsec_dir          = $strongswan::params::strongswan_ipsec_dir,
  $ipsec_conn_dir     = $strongswan::params::strongswan_conn_dir,
  $ipsec_cacerts      = $strongswan::params::strongswan_cacerts,
  $ipsec_private      = $strongswan::params::strongswan_private,
  $ipsec_secrets      = $strongswan::params::strongswan_secrets,
  $ipsec_certs        = $strongswan::params::strongswan_certs,
  $ipsec_secrets_dir  = $strongswan::params::strongswan_secrets_dir,

) inherits strongswan::params {
  include strongswan::install
  include strongswan::config
  include strongswan::network
  include strongswan::firewall
  include strongswan::service
}
