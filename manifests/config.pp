# Class: strongswan::config
#
# Configures the required files and folders required for connections to be made. 
# This doesn't configure a connection
#
# optional parameters.
#
# = $ipsec_conf =
#  The location of the ipsec.conf file
#
# = $ipsec::ipsec_dir =
# The location of the ipsec.d directory 
#
# = $ipsec_cacerts = 
# The location of the certificate auth certs 
#
# = $ipsec_private = 
# The locations for ipsec keys
#
# = $ipsec_certs = 
# The location for ipsec certs
#

class strongswan::config {
  File {
    ensure  => 'directory',
    mode    => '0700',
    owner   => 'root',
    group   => 'root',
    recurse => true,
    purge   => true,
    notify  => Service["$strongswan::service_name"],
  }
  
  file { "$strongswan::ipsec_conf":
    ensure  => 'file',
    mode    => '0600',
    content => template('strongswan/ipsec.conn_default.erb'),
    require => Package["$strongswan::package"],
  }
  
  file {"$strongswan::ipsec_secrets":
    ensure  => 'file',
    mode    => '0600',
    content => template('strongswan/ipsec.secrets_default.erb'),
    require => Package["$strongswan::package"],
  }

  file {"$strongswan::ipsec_dir":
    require => Package["$strongswan::package"],
  }

  file { ["$strongswan::ipsec_cacerts",
          "$strongswan::ipsec_private,",
          "$strongswan::ipsec_conn_dir", 
          "$strongswan::ipsec_secrets_dir",
          "$strongswan::ipsec_certs"]:
    require => File["$strongswan::ipsec_dir"],
  }
}

