# Class Strongswan::params
#
# Parameters used through out the module.
#
# Authors
# -------
#
# Kylie Judd <Kylie.Judd@gmail.com>
#

class strongswan::params {
  case $::osfamily {
    'Redhat': { 
      $strongswan_ipsec       = '/etc/strongswan/ipsec.conf'
      $strongswan_ipsec_dir   = '/etc/strongswan/ipsec.d'
      $strongswan_conn_dir    = '/etc/strongswan/ipsec.d/connections'
      $strongswan_cacerts     = '/etc/strongswan/ipsec.d/cacerts' 
      $strongswan_private     = '/etc/strongswan/ipsec.d/private' 
      $strongswan_secrets     = '/etc/strongswan/ipsec.secrets' 
      $strongswan_secrets_dir = '/etc/strongswan/ipsec.d/secrets'
      $strongswan_certs       = '/etc/strongswan/ipsec.d/certs' 
      $strongswan_conf        = '/etc/strongswan/strongswan.conf' 
      $strongswan_service     = 'strongswan' 
     }
    'debian': { 
      $strongswan_ipsec       = '/etc/ipsec.conf'
      $strongswan_ipsec_dir   = '/etc/ipsec.d'
      $strongswan_conn_dir    = '/etc/ipsec.d/connections'
      $strongswan_cacerts     = '/etc/ipsec.d/cacerts'
      $strongswan_private     = '/etc/ipsec.d/private'
      $strongswan_secrets     = '/etc/ipsec.secrets' 
      $strongswan_secrets_dir = '/etc/ipsec.d/secrets'
      $strongswan_certs       = '/etc/ipsec.d/certs'
      $strongswan_conf        = '/etc/strongswan.conf'
      $strongswan_service     = 'strongswan' 
    }
    default: { fail('os $::operatingsystem not supported') }
  }
}

